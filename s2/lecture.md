autoscale: true

#[fit]Day 2 Session 2

## Decision Theory and Model Comparison

---

#[fit]Decision Theory


*Predictions (or actions based on predictions) are described by a utility or loss function, whose values can be computed given the observed data*.

---

## Custom Loss: Stock Market Returns

![inline, 40%](images/smreturns.png)

```python
def stock_loss(stock_return, pred, alpha = 100.):
    if stock_return * pred < 0:
        #opposite signs, not good
        return alpha*pred**2 - np.sign(stock_return)*pred \
                        + abs(stock_return)
    else:
        return abs(stock_return - pred)
```

![right, fit](images/smreturnsloss.png)


---

## The two risks

There are *two risks in learning* that we must consider, one to *estimate probabilities*, which we call **estimation risk**, and one to *make decisions*, which we call **decision risk**.

The **decision loss** $$l(y,a)$$ or **utility** $$u(l,a)$$ (profit, or benefit) in making a decision $$a$$ when the predicted variable has value $$y$$. For example, we must provide all of the losses $$l$$(no-cancer, biopsy), $$l$$(cancer, biopsy), $$l$$(no-cancer, no-biopsy), and $$l$$(cancer, no-biopsy). One set of choices for these losses may be 20, 0, 0, 200 respectively.

---

## Classification Risk

$$ R_{a}(x) = \sum_y l(y,a(x)) p(y|x)$$

That is, we calculate the **predictive averaged risk** over all choices y, of making choice a for a given data point.

Overall risk, given all the data points in our set:

$$R(a) = \int dx p(x) R_{a}(x)$$

---

## Two class Classification

$$R_a(x) = l(1, g)p(1|x) + l(0, g)p(0|x).$$

Then for the "decision" $$a=1$$ we have:

$$R_1(x) = l(1,1)p(1|x) + l(0,1)p(0|x),$$

and for the "decision" $$a=0$$ we have:

$$R_0(x) = l(1,0)p(1|x) + l(0,0)p(0|x).$$

![left, fit](images/confusionmatrix.png)

---

Now, we'd choose $$1$$ for the data point at $$x$$ if:

$$R_1(x) \lt R_0(x).$$

$$ P(1|x)(l(1,1) - l(1,0)) \lt p(0|x)(l(0,0) - l(0,1))$$

So, to choose '1', the Bayes risk can be obtained by setting:

$$p(1|x) \gt r P(0|x) \implies r=\frac{l(0,1) - l(0,0)}{l(1,0) - l(1,1)}$$

$$P(1|x) \gt t = \frac{r}{1+r}$$.

---

One can use the prediction cost matrix corresponding to the consufion matrix

$$ r  =  =\frac{c_{FP} - c_{TN}}{c_{FN} - c_{TP}}$$

If you assume that True positives and True negatives have no cost, and the cost of a false positive is equal to that of a false positive, then $$r=1$$ and the threshold is the usual intuitive $$t=0.5$$.

![right, fit](images/costmatrix.png)

---


#[fit]COMPARING CLASSIFERS

Telecom customer Churn data set from @YhatHQ[^<]

![inline](/Users/rahul/Desktop/presentationimages/churn.png)

[^<]: http://blog.yhathq.com/posts/predicting-customer-churn-with-sklearn.html

---

#[fit]ROC SPACE[^+]


$$TPR = \frac{TP}{OP} = \frac{TP}{TP+FN}.$$

$$FPR = \frac{FP}{ON} = \frac{FP}{FP+TN}$$

![left, fit](/Users/rahul/Desktop/presentationimages/ROCspace.png)

![inline](/Users/rahul/Desktop/bookimages/confusionmatrix.pdf)

[^+]: this+next fig: Data Science for Business, Foster et. al.

---

![left, fit](/Users/rahul/Desktop/presentationimages/howtoroc.png)

![inline,](/Users/rahul/Desktop/presentationimages/roc-curve.png)

#[fit]ROC Curve


---

#ROC CURVE

- Rank test set by prob/score from highest to lowest
- At beginning no +ives
- Keep moving threshold
- confusion matrix at each threshold

---

#ROC curves

![fit, original](/Users/rahul/Desktop/presentationimages/roccompare.png)

---

#ASYMMETRIC CLASSES

- A has large FP[^#]
- B has large FN. 
- On asymmetric data sets, A will do very bad.
- Both downsampling and unequal classes are used in practice



![fit, left](/Users/rahul/Desktop/bookimages/abmodeldiag.png)


---


#ASYMMETRIC CLASSES


$$r = \frac{l_{FN}}{l_{FP}}$$

We look for lines with slope

$$\frac{p(0)}{r\,p(1)} = \frac{p(-)}{r\,p(+)} \sim \frac{10}{r}$$

Large $$r$$ penalizes FN.

Churn and Cancer u dont want FN: an uncaught churner or cancer patient (P=churn/cancer)

![fit, right](/Users/rahul/Desktop/presentationimages/asyroc2.png)

---

![left, fit](/Users/rahul/Desktop/presentationimages/liftcompare.png)

#Lift Curve

- Rank test set by prob/score from highest to lowest
- Calculate TPR for each confusion matrix ($$TPR$$) 
- Calculate fraction of test set predicted as positive ($$x$$)
- Plot $$Lift = \frac{TPR}{x}$$ vs $$x$$. Lift represents the advantage of a classifier over random guessing.

---

#EXPECTED VALUE FORMALISM

Can be used for risk or profit/utility (negative risk)

$$EP = p(1,1) \ell_{11} + p(0,1) \ell_{10} + p(0,0) \ell_{00} + p(1,0) \ell_{01}$$


$$EP = p_a(1) [TPR\,\ell_{11} + (1-TPR) \ell_{10}]$$
         $$ + p_a(0)[(1-FPR) \ell_{00} + FPR\,\ell_{01}]$$
        
Fraction of test set pred to be positive $$x=PP/N$$:

$$x = (TP+FP)/N = TPR\,p_o(1) + FPR\,p_o(0)$$

---

![fit, right](/Users/rahul/Desktop/presentationimages/profitcompare.png)

#Profit curve

- Rank test set by prob/score from highest to lowest
- Calculate the expected profit/utility for each confusion matrix ($$U$$) 
- Calculate fraction of test set predicted as positive ($$x$$)
- plot $$U$$ against $$x$$

---

#Finite budget[^#]

![fit, right](/Users/rahul/Desktop/presentationimages/pcurves.png)


- 100,000 customers, 40,000 budget, 5$ per customer
- we can target 8000 customers
- thus target top 8%
- classifier 1 does better there, even though classifier 2 makes max profit

[^#]: figure from Data Science for Business, Foster et. al.

